public class TileCoordinates {

    /*
        A TileCoordinates object stores the position of a tile on the board,
        and provides a few methods to calculate screen coordinates

        Coordinates in array on board:
        [0,0]
        [0,1][1,1]
        [0,2][1,2][2,2]
        [0,3][1,3][2,3][3,3]

        arranged like

               [0,0]
            [0,1][1,1]
          [0,2][1,2][2,2]
        [0,3][1,3][2,3][3,3]

        -- converting board coordinates to screen coordinates --
            -- x coordinates --
            screenX = boardX * width;
            horizontal offset is highest at top, lowest at bottom
            horizontal offset factor = 1-(boardY / boardHeight)
            horizontal offset maximum is screenWidth/2
            offset = factor * maximum --> (1-(boardY / boardHeight)) * (screenWidth/2);
            screenX = screenX + offset;
            -- y coordinates --
            vertical offset is the same for every x-value
            vertical offset is the boardY * boardHeight
        -- converting screen coordinates back to board coordinates --
            -- x coordinates --
            boardX = screenX / width;
            horizontal offset is highest at top, lowest at bottom
            horizontal offset factor = 1-(screenY / height)
            horizontal offset maximum is width/2
            hor offset = factor * maximum --> (1-(screenY / height)) * (width/2);
            boardX = boardX - offset;

            -- y coordinates --
            boardY = screenY / height;

     */
    int boardX = 0;
    int boardY = 0;

    // hidden constructor
    TileCoordinates() {

    }

    public boolean isConnected(GameBoard board, TileCoordinates check) {
        return this.insideBoard(board) && check.insideBoard(board) && (Math.abs(check.boardX-this.boardX)==1 && Math.abs(check.boardX-this.boardX)==1);
    }

    public boolean insideBoard(GameBoard board) {
        // (this.boardX<=this.boardY) checks if the x-coord of the cell is smaller than the y-coord, because of the triangular shape that the field is arranged in this must be true
        // (this.boardX>=0 && this.boardY>=0) && (this.boardX<board.width && this.boardY<board.height) makes sure that the cell falls within the bounds of the board (>=0 and <size)
        return (this.boardX<=this.boardY) && (this.boardX>=0 && this.boardY>=0) && (this.boardX<board.width && this.boardY<board.height);
    }

    public boolean isOnEdge(GameBoard board) {
        // First check if this cell lies inside the game board
        // (this.boardX<=this.boardY) checks if the x-coord of the cell is equal than the y-coord, because of the triangular shape that the field is arranged in. If this is true, it must be on the border
        // (this.boardX==board.width-1 || this.boardY==board.height-1) checks if the cell falls exactly onthe bounds of the board (==0 and ==size-1)
        return insideBoard(board) && ((this.boardX==this.boardY) || (this.boardX==0 || this.boardY==0) || (this.boardX==board.width-1 || this.boardY==board.height-1));
    }

    public int getBoardX() {
        return this.boardX;
    }

    public int getBoardY() {
        return this.boardY;
    }

    public int getScreenX(GameBoard board) {
        float screenX = boardX * board.getTileWidth();
        float offset = (1-((float)boardY / (float)board.height)) * (board.screenWidth/2);
        screenX += offset;
        return (int)screenX;
    }

    public int getScreenY(GameBoard board) {
        float screenY = boardY * board.getTileHeight();
        return (int)screenY;
    }

    public static TileCoordinates createFromBoardCoordinates(GameBoard board, int boardX, int boardY) {
        TileCoordinates coords = new TileCoordinates();
        coords.boardX = boardX;
        coords.boardY = boardY;
        return coords;
    }

    public static TileCoordinates createFromScreenCoordinates(GameBoard board, int screenX, int screenY) {
        TileCoordinates coords = new TileCoordinates();

        float boardX, boardY;
        boardY = screenY / board.screenHeight;
        boardX = (float)screenX / (float)board.screenWidth;
        float offset = (1-((float)boardY / (float)board.height)) * (board.screenWidth/2);
        boardX-=offset;

        coords.boardX = (int)boardX;
        coords.boardY = (int)boardY;
        return coords;
    }

    public String toString() {
        return "cell["+this.boardX+","+this.boardY+"];";
    }
}
