import javax.swing.*;

// Main klasse
public class Main {

    // Titel van het programma
    public static String title = "Driehoekding";
    public static JFrame frame;
    // Breedte en hoogte van het speelvenster
    public static int width = 500, height = 500;

    // Main-methode
    public static void main(String[] args) {
        Main.frame = createFrame();
    }

    // Maak het venster aan
    private static JFrame createFrame() {
        JFrame f = new JFrame(Main.title);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(width, height);
        f.add(new GamePanel());
        f.pack();
        f.setVisible(true);
        return f;
    }
}
