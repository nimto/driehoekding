import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GamePanel extends JPanel{

    private GameBoard gameBoard;
    int x = 0,y = 0;

    public GamePanel() {
        setBorder(BorderFactory.createLineBorder(Color.black));
        setGameBoard(new GameBoard(20,20,Main.width, Main.height));

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                getGameBoard().click(e.getX(),e.getY());
                System.out.println("koes "+e.getX()+","+e.getY());
                repaint();
            }
        });
    }

    public GameBoard getGameBoard() {
        return this.gameBoard;
    }

    public void setGameBoard(GameBoard board) {
        this.gameBoard = board;
    }

    public Dimension getPreferredSize() {
        return new Dimension(Main.width,Main.height);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Draw the gameboard
        getGameBoard().draw(g);
    }
}
