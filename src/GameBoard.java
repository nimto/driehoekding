import java.awt.*;

public class GameBoard {

    // board width and height (in tiles), and screen width and height
    public int width, height, screenWidth, screenHeight;
    int clickedX, clickedY;

    // byte array for the playing field
    public byte[][] field;

    public GameBoard(int width, int height, int screenWidth, int screenHeight) {
        this.width = width;
        this.height = height;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.field = new byte[width][height];
    }

    public float getTileWidth() {
        return (float)screenWidth / (float)(width+1);
    }

    public float getTileHeight() {
        return (float)screenHeight / (float)(height+1);
    }

    public void click(int x, int y) {
        TileCoordinates coords = TileCoordinates.createFromScreenCoordinates(this, x, y);
        this.clickedX = coords.getBoardX();
        this.clickedY = coords.getBoardY();
        System.out.println("coords: "+coords);
    }

    public void draw(Graphics g) {
        // iterate through the playing field
        for(int xx=0;xx<width;xx++)
        for(int yy=0;yy<height;yy++) {
            TileCoordinates coords = TileCoordinates.createFromBoardCoordinates(this, xx, yy);
            // only draw if inside
            if (coords.insideBoard(this)) {
                g.drawOval(coords.getScreenX(this),coords.getScreenY(this),(int)this.getTileWidth(),(int)this.getTileHeight());
                if (coords.isOnEdge(this))
                g.fillOval(coords.getScreenX(this),coords.getScreenY(this),(int)this.getTileWidth(),(int)this.getTileHeight());
            }
        }
    }
}
